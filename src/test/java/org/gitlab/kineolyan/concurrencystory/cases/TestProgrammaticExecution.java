package org.gitlab.kineolyan.concurrencystory.cases;

import lombok.val;
import org.gitlab.kineolyan.concurrencystory.ExplicitScheduler;
import org.gitlab.kineolyan.concurrencystory.StoryScheduler;
import org.gitlab.kineolyan.concurrencystory.internal.SimpleSchedulerImpl;
import org.gitlab.kineolyan.concurrencystory.internal.Util;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

class TestProgrammaticExecution {

  @Test
  void testSimple() {
    val s = new SimpleSchedulerImpl();
    val names = IntStream.rangeClosed(1, 3).mapToObj(i -> "t-" + i).toList();
    val ints = new ArrayList<Integer>();
    IntStream.range(0, names.size())
        .forEach(
            tIdx -> {
              val tName = names.get(tIdx);
              s.createThread(
                  tName,
                  sx -> {
                    for (int i = 0; i < 4; i++) {
                      val firstThread = tIdx != 0;
                      val firstTurn = i == 0;
                      if (firstThread && firstTurn) {
                        sx.awaitControl(names.get((names.size() + tIdx - 1) % names.size()));
                      }
                      ints.add(tIdx);
                      val lastTurn = i == 3;
                      if (lastTurn) {
                        sx.releaseControl();
                      } else {
                        sx.transferControlAndAwait(
                            names.get((names.size() + tIdx + 1) % names.size()));
                      }
                    }
                  });
            });
    assertThat(ints).containsExactly(1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3);
  }

  @Test
  void testExplicit() {
    val s = ExplicitScheduler.create();
    val threadCount = 3;
    val turns = 4;
    val tIds = Util.listOf(threadCount, i -> s.createThreadId("t-" + (i + 1)));
    val startCk = s.createStartPoint(tIds, tIds.get(0));
    val cks = Util.listOf(threadCount * turns - 2, i -> s.createTransferPoint(null, null));
    val endCk = s.createEndPoint();
    val values = Collections.synchronizedList(new ArrayList<Integer>());
    Util.listOf(
        threadCount,
        tIdx ->
            s.defineThread(
                tIds.get(tIdx),
                sx -> {
                  for (int i = 0; i < turns; i++) {
                    val firstThread = tIdx != 0;
                    val firstTurn = i == 0;
                    if (firstTurn) {
                      if (firstThread) {
                        sx.awaitToStart(startCk);
                      } else {
                        sx.signalReady(startCk, cks.get(tIdx - 1));
                      }
                    }
                    values.add(tIdx);
                    val lastTurn = i == 3;
                    if (lastTurn) {
                      sx.end(endCk);
                    } else {
                      sx.transferControlUntil(
                          cks.get(threadCount * i + tIdx), cks.get(threadCount * (i + 1)));
                    }
                  }
                }));
    assertThat(values).containsExactly(1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3);
  }

  @Test
  void testStory() {
    val s = StoryScheduler.create();
    val threadCount = 3;
    val turns = 4;
    val tIds = Util.listOf(threadCount, i -> s.createThreadId("t-" + (i + 1)));
    val values = Collections.synchronizedList(new ArrayList<Integer>());
    val story =
        s.defineStory(
            builder -> {
              for (int i = 0; i < turns; i++) {
                for (int t = 0; t < threadCount; t++) {
                  val tIdx = i;
                  builder.runThread(tIds.get(tIdx), ctx -> values.add(tIdx));
                }
              }
              return builder.build();
            });
    story.runOrElseThrow();
    assertThat(values).containsExactly(1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3);
  }
}
