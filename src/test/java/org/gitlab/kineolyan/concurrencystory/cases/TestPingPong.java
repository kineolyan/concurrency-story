package org.gitlab.kineolyan.concurrencystory.cases;

import lombok.val;
import org.gitlab.kineolyan.concurrencystory.ExplicitScheduler;
import org.gitlab.kineolyan.concurrencystory.StoryScheduler;
import org.gitlab.kineolyan.concurrencystory.internal.SimpleSchedulerImpl;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class TestPingPong {
  @Test
  void testSimple() {
    val s = new SimpleSchedulerImpl();
    val values = Collections.synchronizedList(new ArrayList<>());
    s.createThread(
        "t1",
        sx -> {
          sx.awaitControl("t2");
          values.add("ping-1");
          sx.transferControlAndAwait("t2");
          values.add("ping-2");
          sx.transferControlAndAwait("t2");
          values.add("ping-3");
          sx.releaseControl();
        });
    s.createThread(
        "t2",
        sx -> {
          sx.transferControlAndAwait("t1");
          values.add("pong-1");
          sx.transferControlAndAwait("t1");
          values.add("pong-2");
          sx.transferControlAndAwait("t1");
          values.add("pong-3");
          sx.releaseControl();
        });
    assertThat(values).containsExactly("ping-1", "pong-1", "ping-2", "pong-2", "ping-3", "pong-3");
  }

  @Test
  void testExplicit() {
    val s = ExplicitScheduler.create();
    val tId1 = s.createThreadId("t1");
    val tId2 = s.createThreadId();
    val values = Collections.synchronizedList(new ArrayList<>());
    val startCk =
        s.createStartPoint(
            // FIXME: this may not be needed, because we need to know who is taking the lead
            // through the API
            List.of(tId1, tId2), tId1);
    val ck2 = s.createTransferPoint(tId1, tId2);
    val ck3 = s.createTransferPoint(tId2, tId1);
    val ck4 = s.createTransferPoint(tId1, tId2);
    val ck5 = s.createTransferPoint(tId2, tId1);
    val endCk = s.createEndPoint();
    s.defineThread(
        tId1,
        sx -> {
          sx.awaitToStart(startCk);
          values.add("ping-1");
          sx.transferControlUntil(ck2, ck3);
          values.add("ping-2");
          sx.transferControlUntil(ck4, ck5);
          values.add("ping-3");
          sx.end(endCk);
        });
    s.defineThread(
        tId2,
        sx -> {
          sx.signalReady(startCk, ck2);
          values.add("ping-1");
          sx.transferControlUntil(ck3, ck4);
          values.add("ping-2");
          sx.transferControlUntil(ck5, endCk.endOf(tId1));
          values.add("ping-3");
          sx.end(endCk);
        });
    assertThat(values).containsExactly("ping-1", "pong-1", "ping-2", "pong-2", "ping-3", "pong-3");
  }

  @Test
  void testStory() {
    val s = StoryScheduler.create();
    val t1 = s.createThreadId();
    val t2 = s.createThreadId();
    val values = Collections.synchronizedList(new ArrayList<>());
    val story =
        s.defineStory(
            builder ->
                builder
                    .initialize(
                        Map.of(
                            t1, id -> System.out.println("starting " + id),
                            t2, id -> System.out.println("Concurrently starting " + id)))
                    .runThread(t1, ctx -> values.add("ping-1"))
                    .runThread(t2, ctx -> values.add("pong-1"))
                    .runThread(t1, ctx -> values.add("ping-2"))
                    .runThread(t2, ctx -> values.add("pong-2"))
                    .runThread(t1, ctx -> values.add("ping-3"))
                    .runThread(t2, ctx -> values.add("pong-3"))
                    .build());
    story.runOrElseThrow();
    assertThat(values).containsExactly("ping-1", "pong-1", "ping-2", "pong-2", "ping-3", "pong-3");
  }
}
