package org.gitlab.kineolyan.concurrencystory.cases;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import lombok.val;
import org.gitlab.kineolyan.concurrencystory.internal.SimpleSchedulerImpl;
import org.junit.jupiter.api.Test;

class TestLocks {

  @Test
  void testReentrantLock() {
    val s = new SimpleSchedulerImpl();
    val lock = new ReentrantLock();
    s.createThread(
        "t1",
        sx -> {
          sx.awaitControl("t2");
          sx.runBlockingOperation(
              ctx -> {
                lock.lock();
                try {
                  ctx.checkOthersCannotProgress();
                } finally {
                  lock.unlock();
                }
              });
        });
    val counter = new AtomicInteger(0);
    val reached = new AtomicInteger(0);
    s.createThread(
        "t2",
        sx -> {
          sx.transferControlAndAwait("t1");
          sx.runBlockedOperation(
              ctx -> {
                counter.incrementAndGet();
                lock.lock();
                try {
                  ctx.reached();
                  reached.incrementAndGet();
                } finally {
                  lock.unlock();
                }
              });
        });
    // Future assertions for the framework
    // Check that the checkpoint is correctly check, because the framework run first to check for a
    // block, then check that the block is properly released after
    assertThat(counter.get()).isEqualTo(1);
    assertThat(reached.get()).isEqualTo(1);
  }
}
