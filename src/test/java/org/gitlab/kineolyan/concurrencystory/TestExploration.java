package org.gitlab.kineolyan.concurrencystory;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import lombok.val;
import org.gitlab.kineolyan.concurrencystory.internal.SimpleSchedulerImpl;
import org.junit.jupiter.api.Test;

class TestExploration {

  @Test
  void threadExecution() {
    val step1 = new CountDownLatch(1);
    val step2 = new CountDownLatch(1);
    aThread(
        "t1",
        () -> {
          System.out.println("some action");
          step1.countDown();
          val success = step2.await(1, TimeUnit.HOURS);
          assertThat(success).isTrue();
          System.out.println("more action");
        });

    aThread(
        "t2",
        () -> {
          System.out.println("started but waiting for t1 to start as well");
          val success = step1.await(1, TimeUnit.HOURS);
          System.out.println("Doing real work");
          step2.countDown();
        });

    // Conclusion:
    // As many countdown latch needed as there are transfer of control between threads.
  }

  @Test
  void threadExecutionALot() {
    val steps = List.<CountDownLatch>of();
    aThread(
        "t1",
        () -> {
          System.out.println("started but waiting for t2 to start as well");
          steps.get(0).await(1, TimeUnit.HOURS);
          System.out.println("Doing real work");
          steps.get(1).countDown();
          steps.get(2).await();
          System.out.println("t2 completed its part");
          steps.get(3).countDown();
        });

    aThread(
        "t2",
        () -> {
          System.out.println("some action");
          steps.get(0).countDown();
          val success = steps.get(1).await(1, TimeUnit.HOURS);
          assertThat(success).isTrue();
          System.out.println("more action");
          steps.get(2).countDown();
          steps.get(3).await();
          System.out.println("All work is done");
        });
    // Conclusion:
    // As many countdown latch needed as there are transfer of control between threads.
  }

  @Test
  void threadExecutionALotWithApi() {
    val s = new SimpleSchedulerImpl();
    s.createThread(
        "t1",
        sx -> {
          System.out.println("started but waiting for t2 to start as well");
          sx.awaitControl("t2");
          System.out.println("Doing real work");
          sx.transferControlAndAwait("t2");
          System.out.println("t2 completed its part");
          sx.releaseControl();
        });

    s.createThread(
        "t2",
        sx -> {
          System.out.println("some action");
          sx.transferControlAndAwait("t1");
          System.out.println("more action");
          sx.transferControlAndAwait("t2");
          System.out.println("All work is done");
        });
  }

  @Test
  void threadBlocked() {
    val step1 = new CountDownLatch(1);
    val step2 = new CountDownLatch(1);
    val step3 = new CountDownLatch(1);
    val lock = new ReentrantLock();
    aThread(
        "t1",
        () -> {
          System.out.println("preparing to lock something, waiting for t2");
          step1.await();
          lock.lock();
          try {
            step2.countDown();
            val enteredLock = step3.await(1, TimeUnit.MINUTES);
            assertThat(enteredLock).isFalse();
          } finally {
            lock.unlock();
          }
          // Now t2 can operate
          val lockAfterRelease = step3.await(1, TimeUnit.MINUTES);
          assertThat(lockAfterRelease).isTrue();
          // The above should always be tested, by t1 or by the framework in general
        });

    aThread(
        "t2",
        () -> {
          step1.countDown();
          step2.await();
          lock.lock();
          try {
            step3.countDown();
          } finally {
            lock.unlock();
          }
        });
    // Conclusion:
    // 3 countdowns needed to check that the second thread did start, then the first did the
    // blocking call, then that second still cannot pass the barrier
  }

    private static void aThread(String name, ThrowingRunnable action) {
    // ignore
  }
}
