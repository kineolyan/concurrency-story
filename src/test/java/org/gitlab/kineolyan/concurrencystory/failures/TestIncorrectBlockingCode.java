package org.gitlab.kineolyan.concurrencystory.failures;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.StampedLock;
import lombok.val;
import org.gitlab.kineolyan.concurrencystory.internal.SimpleSchedulerImpl;
import org.junit.jupiter.api.Test;

class TestIncorrectBlockingCode {

  @Test
  void testFailingBlock() {
    val ts = new SimpleSchedulerImpl();
    val lock = new StampedLock();
    val stamp = new AtomicLong(0);
    ts.createThread(
        "t1",
        sx -> {
          sx.awaitControl("t2");
          sx.runBlockingOperation(
              ctx -> {
                stamp.set(lock.tryReadLock());
                try {
                  assertThat(stamp.get()).isNotZero();
                  ctx.checkOthersCannotProgress();
                } finally {
                  val v = stamp.getAndSet(0);
                  if (v != 0) {
                    lock.unlock(v);
                  }
                }
              });
        });
    ts.createThread(
        "t2",
        sx -> {
          sx.transferControlAndAwait("t1");
          sx.runBlockedOperation(
              ctx -> {
                val readStamp = stamp.getAndSet(0);
                val writeStamp = lock.tryConvertToWriteLock(readStamp);
                try {
                  assertThat(writeStamp).isNotZero();
                  stamp.set(writeStamp);
                  ctx.reached();
                } finally {
                  val v = stamp.getAndSet(0);
                  if (v != 0) {
                    lock.unlock(0);
                  }
                }
              });
        });
    // FIXME: find that we have some errors
    val errors = ts.waitAndGetFinalErrors();
    assertThat(errors).hasSize(1).extracting(Map.Entry::getKey).containsExactly("t1");
    // FIXME: we must use a special type of errors
    assertThat(errors.get(0).getValue()).isInstanceOf(RuntimeException.class);
  }
}
