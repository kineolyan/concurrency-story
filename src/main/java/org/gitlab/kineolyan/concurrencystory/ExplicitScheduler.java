package org.gitlab.kineolyan.concurrencystory;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface ExplicitScheduler {

  ThreadId createThreadId();

  ThreadId createThreadId(String label);

  StartPoint createStartPoint(List<ThreadId> awaited, ThreadId starting);

  TransferPoint createTransferPoint(ThreadId from, ThreadId to);

  EndPoint createEndPoint();

  ThreadHandle defineThread(ThreadId id, Consumer<ThreadContext> body);

  List<Map.Entry<String, Throwable>> waitAndGetFinalErrors();

  static ExplicitScheduler create() {
    throw new UnsupportedOperationException("todo");
  }

  interface StoryMark {}

  interface StartPoint extends StoryMark {}

  interface TransferPoint extends StoryMark {}

  interface EndPoint {

    StoryMark endOf(ThreadId id);
  }

  interface ThreadContext {
    void awaitToStart(StartPoint ck);

    void signalReady(StartPoint ck, StoryMark mark);

    void transferControlUntil(TransferPoint ck, StoryMark mark);

    void end(EndPoint ctx);
  }
}
