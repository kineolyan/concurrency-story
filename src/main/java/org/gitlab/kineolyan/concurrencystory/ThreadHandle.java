package org.gitlab.kineolyan.concurrencystory;

public interface ThreadHandle {
  void start();

}
