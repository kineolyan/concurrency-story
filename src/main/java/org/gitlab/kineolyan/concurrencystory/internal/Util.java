package org.gitlab.kineolyan.concurrencystory.internal;

import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Util {
  private Util() {}

  public static <T> List<T> listOf(final int count, IntFunction<T> creator) {
    return IntStream.range(0, count).mapToObj(creator).toList();
  }

  public static int nextInSeq(final int count, final int i) {
    return (count + i + 1) % count;
  }

  public static int prevInSeq(final int count, final int i) {
    return (count + i - 1) % count;
  }
}
