package org.gitlab.kineolyan.concurrencystory.internal;

import org.gitlab.kineolyan.concurrencystory.SimpleScheduler;
import org.gitlab.kineolyan.concurrencystory.ThreadHandle;
import org.gitlab.kineolyan.concurrencystory.ThrowingConsumer;

import java.util.List;
import java.util.Map;

public class SimpleSchedulerImpl implements SimpleScheduler {

  @Override
  public ThreadHandle createThread(final String name, ThrowingConsumer<ThreadContext> body) {
    throw new UnsupportedOperationException("todo");
  }

  @Override
  public List<Map.Entry<String, Throwable>> waitAndGetFinalErrors() {
    throw new UnsupportedOperationException("todo");
  }
}
