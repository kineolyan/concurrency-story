package org.gitlab.kineolyan.concurrencystory;

@FunctionalInterface
public interface ThrowingConsumer<T> {
  void accept(T t) throws Exception;
}
