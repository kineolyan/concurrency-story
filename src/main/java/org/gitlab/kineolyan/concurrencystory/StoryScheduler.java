package org.gitlab.kineolyan.concurrencystory;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface StoryScheduler {
  ThreadId createThreadId();

  ThreadId createThreadId(String label);

  static StoryScheduler create() {
    throw new UnsupportedOperationException("todo");
  }

  Story defineStory(Function<StoryBuilder, StorySchedule> o);

  interface StoryBuilder {

    StoryBuilder initialize(Map<ThreadId, ThrowingConsumer<ThreadId>> actions);

    StoryBuilder runThread(ThreadId id, ThrowingConsumer<ExecutionContext> action);

    StorySchedule build();
  }

  interface StorySchedule {}

  interface Story {

    void runOrElseThrow();

    List<Map.Entry<ThreadId, Throwable>> run();
  }

  interface ExecutionContext {}
}
