package org.gitlab.kineolyan.concurrencystory;

@FunctionalInterface
public interface ThrowingRunnable {
  void run() throws Exception;
}
