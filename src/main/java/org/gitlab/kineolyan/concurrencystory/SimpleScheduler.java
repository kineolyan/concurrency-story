package org.gitlab.kineolyan.concurrencystory;

import java.util.List;
import java.util.Map;

public interface SimpleScheduler {
  ThreadHandle createThread(String name, ThrowingConsumer<ThreadContext> body);

  List<Map.Entry<String, Throwable>> waitAndGetFinalErrors();

  interface BlockingContext {
    void checkOthersCannotProgress();
  }

  interface BlockedContext {
    void reached();
  }

  interface ThreadContext {
    void transferControlAndAwait(final String threadId);

    /** Awaits for other threads to be ready and to give control to this thread. */
    void awaitControl(final String... threadIds);

    void releaseControl();

    void runBlockingOperation(ThrowingConsumer<BlockingContext> action);

    void runBlockedOperation(ThrowingConsumer<BlockedContext> action);
  }
}
